// [SECTION] Comments

// Comments are parts of the code that gets ignored by the language.
// Comments are meant to describe the written code.

// There are two types of comments:
//  - Single line comment (ctrl+/) denoted by two slashes (//)
/*
	- Multiline comment
	- denoted by / ** /
	- ctrl + shift + /
*/

// [SECTION] Statements and Syntax
	// Statements
		// In programming language, are instructions that we tell the computer to perform
		// JS statements usually end with semicolon(;).
			// (;) is also called as delimeter.
		// Semicolons are not required in JS.
			// it is use to help us train or locate where a statement ends.
	// Sytax
		// In programming, it is the set of rules that describe how statements must be constructed.

console.log("Hellow po");

// [SECTION] Variables

// it is used to contain data.

//  Declaring a variable
	// tells out devices that a variable name is created and is ready to strore data.

	// Systanx: let/const variableName;

	// let is a keyword that is used in declaring a varable.
	let myVariable;
	console.log(myVariable); //useful for printing values of a variable

	//console.log(Hello); //error: not defined

	// Initialize a value
		// Storing the initial value of a variable.
		// Assignment Operator(=)
	myVariable = "Jimmy";

	// Reassignment a variable  value
		// Changing the itinial value to a new value
	myVariable = "Jimmy Poge";
	console.log(myVariable);
	
	// const
		//"const" keyword is used to declare and initialized a constant variable
		// A "constant" variable does not change.
	
	// const PI;
	// PI = 3.1416;
	// console.log(PI); //error: Initial value is missing for const.

// Declaring with initialilzation
	// a variable is given its initial/starting value upon declaration.
		// Syntax: let/const variableName = value;

		let productName = "Desktop Computer";
		console.log(productName);

		let productPrice = 18999;
		// let productPrice = 20000; error: variable has been declared
		console.log(productPrice);

		// const keyword
		const PI = 3.1416;
		//PI = 3.14; error: assignment to constant variable.
		console.log(PI);

/*
	Guide in writing variables.
	1. Use the "let" keyword if the variable will contain different values/can change overtime.
		Naming Convention: Variable name should starts with "lowercase characters" and "camelCasing" is use for multiple words. 
	2. Use the "const" keyword if the variable does not changed.
		Naming Convention: "const" keyword shold be followed by ALL CAPITAL VARIABLE NAME (single value variable).

	3. Variable names should be indicative (or descriptive) of the value being stored to  avoid confusion.

	Avoid this one: let word = "John Smith"

	4. Variable name are case sensitive.
*/

// Multiple variable declaration
	
	let productCode = "DC017", productBrand = "Dell";
	console.log(productCode,productBrand);

// Using a reserve keyword
	
	// const let = "Hello";
	// console.log(let); error: lexically bound is disallowed.	

// [SECTION] Data Types

// In Javascript, there are six types of data

	// String
		//are series of characters that create a word, a phrase, a sentence or anything related to creating text.
		// Strings in JavaScsript can be written using a single quote ('') or double quote ("")

	let country = "Philippines";
	let province = 'Metro Manila';

	// Concatenating Strings
	// Multiple string values that can be combined to create a single String using the "+" symbol.
	// I live in Metro Manila, Philippines
	let greeting = "I live in " + province + ", " + country;
	console.log(greeting);

	// Escape Characters
	//  (\) in string combination with other characters can produce different result
		// "\n" referst to creating a new line
	let mailAddress = "Quezon City\nPhilippines";
	console.log(mailAddress);

	// Expected output: John's employees went home early.
	let message = "John's employees went home early";
	console.log(message);

	// Using the escape charaacters (\)
	message = 'Jane\'s employees went home early.';
	console.log(message);

// Numbers
	// includes positive, negative numbers with decimal places.
	// Integers/Whole Numbers
	let headcount = 26;
	console.log(headcount);

	// Decimal Numbers/Fractions
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// combine number and strings
	console.log("John's grade last quarter is " + grade)

// Boolean
	// Boolean values are normally used to store values relating to the state of certain things.
	// true or false (two logical values)

	let isMarried = false;
	let isGoodConduct = true;

	console.log("isMarried: " + isMarried);
	console.log("isGoodConduct: " + isGoodConduct);

// Array
	//are special kind of data type that can used to store multiple ralated values.
	// Syntax: let/const arrayName = [elementA, elementB,... elementNth];

	let grades = [98.7 , 92.1, 90.2, 94.6];
	// constant variable cannot be reassigned
	// grades = 100; // not allowed
	// changing the elements of an array or changing the properties of an object is allowed in constant variable
	grades[0] = 100;
	console.log(grades);


	// This will work, but not recommended
	let details = ["John", "Smith", 32, true];
	console.log(details);

// Objects
	// Objects are another special kind of data type that is used to mimic real world objects/items.
	// used to create complex data that contains information releant to each other.
	/*
		Syntax:
			let/const objectName = {
				propertyA: valueA,
				propertyB: valueB				
			}

	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried:false,
		contact: ["+63912 345 6789","8123 456"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	};
	console.log(person);

	// They're are also useful for creating abstract objects
		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		};

		console.log(myGrades);

		// typeof operator is used to determined the type of data or the value of a variable.

		console.log(typeof myGrades); //object

		// arrays is a special type of object with methods.
		console.log(typeof grades); //object


// Null
	// indicates the absence of value
	let spouse = null;
	console.log(spouse);

// Undifined
	// indicates that a variables has not been given a vale yet.
	let fullName;
	console.log(fullNames);